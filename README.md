Cloud face recognition REST-client (GUI & CLI, multithreading loading & server response handling) for https://docs.facecloud.tevian.ru/:
�	Files or folder selection & request parameters definition via GUI or CLI
�	Request for face recognition with gender determination & age estimation
�	Recognition results are issued as JSON file (CLI) output or drawn at initial image
�	Images scaling & moving
