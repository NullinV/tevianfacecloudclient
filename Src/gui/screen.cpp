#include <QDebug>
#include <QMouseEvent>
#include <QPainter>
#include <QWheelEvent>

#include "screen.h"

Screen::Screen(QWidget *parent) : QFrame(parent) {

  setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);

  setFrameStyle(QFrame::Panel | QFrame::Raised);
  setMidLineWidth(1);
}

void Screen::paintEvent(QPaintEvent *event) {

  /* The place is here according to
   * https://forum.qt.io/topic/36299/some-questions-about-accept-and-ignore-of-event/6
   */
  QFrame::paintEvent(event);

  if (!image.isNull()) {

    QPixmap canvas(size());
    canvas.fill(Qt::transparent);
    QRectF target(hMargin, vMargin, image.width() * scaleFactor,
                  image.height() * scaleFactor);
    QPainter canvasPainter(&canvas);
    canvasPainter.drawImage(target, image);

    QPainter screenPainter(this);
    target = QRectF(0, 0, width(), height());
    QRectF source(x1, y1, qFloor(width() / zoomFactor),
                  qFloor(height() / zoomFactor));
    screenPainter.drawPixmap(target, canvas, source);

    for (const auto &i : faces) {
      FaceData transformed;
      i.transform(transformed, hMargin - x1, vMargin - y1, scaleFactor,
                  zoomFactor);

      QString age("age: ");
      age.append(QString::number(i.age));
      QString gender("gender: ");
      gender.append(i.gender);

      QPoint agePoint(transformed.x, transformed.y + transformed.height + 20);
      QPoint genderPoint(transformed.x, agePoint.y() + 20);

      screenPainter.drawRect(transformed.x, transformed.y, transformed.height,
                             transformed.width);
      screenPainter.drawText(agePoint, age);
      screenPainter.drawText(genderPoint, gender);
    }
  }
}

void Screen::setMargins() {

  scaleFactor = image.isNull() || image.width() == 0 || image.height() == 0
                    ? 1
                    : qMin(static_cast<double>(width()) / image.width(),
                           static_cast<double>(height()) / image.height());
  hMargin = qMax(0, qFloor((width() - image.width() * scaleFactor) / 2));
  vMargin = qMax(0, qFloor((height() - image.height() * scaleFactor) / 2));
}

void Screen::setImage(ImageData imageData) {

  if (!imageData.image.isNull()) {
    image = imageData.image;

    x1 = y1 = 0;
    zoomFactor = 1;
    setMargins();

    faces.clear();
    faces += imageData.faces;

    moving = false;

    update();
  }
}

#if QT_CONFIG(wheelevent)
void Screen::wheelEvent(QWheelEvent *event) {
  const int degrees = event->delta() / 8;
  int steps = degrees / 15;

  double dx = width() / zoomFactor / 2;
  double dy = height() / zoomFactor / 2;

  zoomFactor = qMax(1.0, qMin(MAX_ZOOM_FACTOR, zoomFactor / pow(0.95, steps)));
  dx -= width() / zoomFactor / 2;
  dy -= height() / zoomFactor / 2;

  x1 += qFloor(dx);
  y1 += qFloor(dy);

  fixCoordinates(x1, y1);

  update();
}
#endif

void Screen::mousePressEvent(QMouseEvent *event) {
  if (event->button() == Qt::LeftButton) {
    x0 = x1;
    y0 = y1;
    lastPos = event->pos();
    moving = true;
  }
}

void Screen::mouseReleaseEvent(QMouseEvent *event) {

  if (event->button() == Qt::LeftButton && moving) {
    if (lastPos != event->pos()) {
      moveRefPoint(event->pos());
      update();
    }
    moving = false;
  }
}
void Screen::mouseMoveEvent(QMouseEvent *event) {
  if ((event->buttons() & Qt::LeftButton) && moving) {
    moveRefPoint(event->pos());
    update();
  }
}

void Screen::moveRefPoint(QPoint pos) {

  x1 = x0 + qFloor((lastPos.x() - pos.x()) / zoomFactor);

  y1 = y0 + qFloor((lastPos.y() - pos.y()) / zoomFactor);
  fixCoordinates(x1, y1);
}

void Screen::resizeEvent(QResizeEvent *event) {

  if (!(image.isNull() || image.width() == 0 || image.height() == 0)) {

    double xI = (x1 - hMargin) / scaleFactor;
    double yI = (y1 - vMargin) / scaleFactor;

    setMargins();

    x1 = qFloor((xI * scaleFactor + hMargin));
    y1 = qFloor((yI * scaleFactor + vMargin));

    fixCoordinates(x1, y1);
  }
  QFrame::resizeEvent(event);
}

void Screen::fixCoordinates(int &x, int &y) {

  if (width() / zoomFactor < width() - 2 * hMargin)
    x = qMax(hMargin,
             qMin(x, qFloor(width() - width() / zoomFactor - hMargin)));
  else
    x = qFloor((width() - width() / zoomFactor) / 2);

  if (height() / zoomFactor < height() - 2 * vMargin)
    y = qMax(vMargin,
             qMin(y, qFloor(height() - height() / zoomFactor - vMargin)));
  else
    y = qFloor((height() - height() / zoomFactor) / 2);
}
