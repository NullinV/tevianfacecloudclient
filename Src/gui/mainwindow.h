#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QJsonObject>
#include <QMainWindow>
#include <QMutex>
#include <QProgressBar>
#include <QVector>

#include "imagedata.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

signals:
  void detectFromFiles(QStringList files, int FDmin, int FDdmax,
                       double threshold);
  void detectFromFolder(QString folder, int FDmin, int FDmax, double threshold);
  void showImage(ImageData imageData);
  void imageIsAdded(ImageData imageData);

protected:
  void closeEvent(QCloseEvent *event) override;

private slots:
  void on_selectSourceButton_clicked();
  void on_detectButton_clicked();
  void setProgressBar(int maximum);
  void addImage(QString path, QJsonObject faces, QImage image);
  void stepForward();
  void handleNewImage(ImageData imageData);

  bool changeSourceType();

private:
  void writeSettings();
  void readSettings();

  Ui::MainWindow *ui = nullptr;
  QProgressBar *progressBar = nullptr;
  QString lastAnotherTypeSource;

  int index;
  QVector<ImageData> images;

  QMutex imagesMutex;
};

#endif // MAINWINDOW_H
