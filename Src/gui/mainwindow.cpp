#include <QCloseEvent>
#include <QFileDialog>
#include <QJsonArray>
#include <QSettings>
#include <QThread>

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "detector.h"
#include "screen.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {

  qRegisterMetaType<ImageData>();
  Screen *screen = new Screen(this);

  screen->setMinimumWidth(100);
  screen->setMinimumHeight(100);
  ui->setupUi(this);
  this->setCentralWidget(ui->CentralWidget);

  ui->screenPlace->addWidget(screen);

  readSettings();

  progressBar = new QProgressBar(this);
  statusBar()->addPermanentWidget(progressBar);
  progressBar->setHidden(true);

  ui->FDmax->setMinimum(ui->FDmin->value());
  ui->FDmin->setMaximum(ui->FDmax->value());

  QThread *thread = new QThread(this);
  Detector *detector;
  detector = new Detector(ui->url->text(), ui->token->toPlainText());
  detector->moveToThread(thread);
  thread->start();

  connect(ui->closeButton, &QPushButton::clicked, this, &MainWindow::close);
  connect(ui->closeButton, &QPushButton::clicked, thread, [thread]() {
    thread->quit();
    thread->wait();
  });
  connect(thread, &QThread::finished, detector, &Detector::deleteLater);

  connect(ui->fromFiles, &QRadioButton::clicked, this,
          &MainWindow::changeSourceType);
  connect(ui->fromFolder, &QRadioButton::clicked, this,
          &MainWindow::changeSourceType);

  connect(ui->FDmin, qOverload<int>(&QSpinBox::valueChanged), this,
          [this](int val) { ui->FDmax->setMinimum(val); });
  connect(ui->FDmax, qOverload<int>(&QSpinBox::valueChanged), this,
          [this](int val) { ui->FDmin->setMaximum(val); });

  connect(ui->token, &QTextEdit::textChanged, detector, [this, detector]() {
    detector->setToken(this->ui->token->toPlainText());
  });

  connect(this, &MainWindow::detectFromFiles, detector,
          qOverload<QStringList, int, int, double>(&Detector::detect));
  connect(this, &MainWindow::detectFromFolder, detector,
          qOverload<QString, int, int, double>(&Detector::detect));

  connect(detector, &Detector::counted, this, &MainWindow::setProgressBar);
  connect(detector, &Detector::detectionFail, this, &MainWindow::stepForward);
  connect(detector, &Detector::obtained, this, &MainWindow::addImage,
          Qt::DirectConnection);
  connect(this, &MainWindow::imageIsAdded, this, &MainWindow::stepForward);
  connect(this, &MainWindow::imageIsAdded, this, &MainWindow::handleNewImage);

  connect(this, &MainWindow::showImage, screen, &Screen::setImage);

  connect(ui->firstButton, &QPushButton::clicked, this, [this]() {
    QMutexLocker locker(&imagesMutex);
    index = 1;
    ui->firstButton->setEnabled(false);
    ui->previousButton->setEnabled(false);
    ui->nextButton->setEnabled(true);
    ui->lastButton->setEnabled(true);
    ui->currentLabel->setText(QString::number(index));
    ui->path->setText(images[index - 1].path);
    emit showImage(images[index - 1]);
  });
  connect(ui->previousButton, &QPushButton::clicked, this, [this]() {
    QMutexLocker locker(&imagesMutex);
    if (--index == 1) {
      ui->firstButton->setEnabled(false);
      ui->previousButton->setEnabled(false);
    }
    ui->nextButton->setEnabled(true);
    ui->lastButton->setEnabled(true);
    ui->currentLabel->setText(QString::number(index));
    ui->path->setText(images[index - 1].path);
    emit showImage(images[index - 1]);
  });
  connect(ui->nextButton, &QPushButton::clicked, this, [this]() {
    QMutexLocker locker(&imagesMutex);
    if (++index == images.size()) {
      ui->nextButton->setEnabled(false);
      ui->lastButton->setEnabled(false);
    }
    ui->firstButton->setEnabled(true);
    ui->previousButton->setEnabled(true);
    ui->currentLabel->setText(QString::number(index));
    ui->path->setText(images[index - 1].path);
    emit showImage(images[index - 1]);
  });
  connect(ui->lastButton, &QPushButton::clicked, this, [this]() {
    QMutexLocker locker(&imagesMutex);
    index = images.size();
    ui->nextButton->setEnabled(false);
    ui->lastButton->setEnabled(false);
    ui->firstButton->setEnabled(true);
    ui->previousButton->setEnabled(true);
    ui->currentLabel->setText(QString::number(index));
    ui->path->setText(images[index - 1].path);
    emit showImage(images[index - 1]);
  });
}

MainWindow::~MainWindow() { delete ui; }

bool MainWindow::changeSourceType() {
  QString temp = ui->source->text();
  ui->source->setText(lastAnotherTypeSource);
  lastAnotherTypeSource = temp;
}

void MainWindow::on_selectSourceButton_clicked() {

  if (ui->fromFiles->isChecked())
    ui->source->setText(
        QFileDialog::getOpenFileNames(this, tr("Select files"), "").join(";"));
  else
    ui->source->setText(QFileDialog::getExistingDirectory(
        this, tr("Open images directory"), "", QFileDialog::ShowDirsOnly));
}
#include <QThread>
void MainWindow::on_detectButton_clicked() {

  images.clear();
  progressBar->setValue(0);
  progressBar->setVisible(false);
  ui->performed->setText("");
  ui->currentLabel->setText("");
  ui->detectButton->setEnabled(false);
  ui->nextButton->setEnabled(false);
  ui->previousButton->setEnabled(false);
  ui->firstButton->setEnabled(false);
  ui->lastButton->setEnabled(false);

  if (ui->fromFiles->isChecked())
    emit detectFromFiles(ui->source->text().split(";"), ui->FDmin->value(),
                         ui->FDmax->value(), ui->threshold->value());
  else
    emit detectFromFolder(ui->source->text(), ui->FDmin->value(),
                          ui->FDmax->value(), ui->threshold->value());
  emit showImage({});
}

void MainWindow::setProgressBar(int maximum) {

  if (maximum > 1) {
    progressBar->setMaximum(maximum - 1);
    progressBar->setVisible(true);
  } else if (maximum == 0) {
    ui->performed->setText("There is nothing to perform");
    ui->detectButton->setEnabled(true);
  }
}

void MainWindow::addImage(QString path, QJsonObject facesData, QImage image) {

  ImageData imageData;
  QJsonArray faces = facesData["data"].toArray();

  imageData.image = image;
  imageData.path = path;

  for (auto i : faces) {
    FaceData face;
    QJsonObject bbox = i.toObject()["bbox"].toObject();
    QJsonObject demographics = i.toObject()["demographics"].toObject();

    face.x = bbox["x"].toInt();
    face.y = bbox["y"].toInt();
    face.height = bbox["height"].toInt();
    face.width = bbox["width"].toInt();
    face.age = demographics["age"].toObject()["mean"].toDouble();
    face.gender = demographics["gender"].toString();

    imageData.faces.push_back(face);
  }

  imagesMutex.lock();
  images.push_back(imageData);
  imagesMutex.unlock();

  emit imageIsAdded(imageData);
}

void MainWindow::stepForward() {

  if (progressBar->value() + 1 == progressBar->maximum())
    ui->detectButton->setEnabled(true);
  ui->performed->setText(
      QString("Performed : %1").arg(progressBar->value() + 1));
  progressBar->setValue(progressBar->value() + 1);
}

void MainWindow::handleNewImage(ImageData imageData) {
  QMutexLocker locker(&imagesMutex);
  if (images.size() == 1) {
    index = 1;
    ui->currentLabel->setText(QString::number(index));
    ui->path->setText(images[index - 1].path);
    emit showImage(imageData);
  } else {
    ui->nextButton->setEnabled(true);
    ui->lastButton->setEnabled(true);
  }
}

void MainWindow::closeEvent(QCloseEvent *event) {
  writeSettings();
  event->accept();
}

void MainWindow::readSettings() {
  QSettings settings;
  settings.beginGroup("MainWindow");
  ui->url->setText(
      settings.value("url", "https://backend.facecloud.tevian.ru/api/v1/detect")
          .toString());
  ui->token->setText(settings.value("token").toString());
  ui->source->setText(settings.value("source").toString());
  lastAnotherTypeSource = settings.value("lastAnotherTypeSource").toString();
  ui->fromFiles->setChecked(settings.value("fromFiles", true).toBool());
  ui->threshold->setValue(settings.value("threshold", 0.8).toDouble());
  ui->FDmin->setValue(settings.value("FDmin", 40).toInt());
  ui->FDmax->setValue(settings.value("FDmax", 500).toInt());
}
void MainWindow::writeSettings() {
  QSettings settings;

  settings.beginGroup("MainWindow");
  settings.setValue("url", ui->url->text());
  settings.setValue("token", ui->token->toPlainText());
  settings.setValue("source", ui->source->text());
  settings.setValue("lastAnotherTypeSource", lastAnotherTypeSource);
  settings.setValue("fromFiles", ui->fromFiles->isChecked());
  settings.setValue("threshold", ui->threshold->value());
  settings.setValue("FDmin", ui->FDmin->value());
  settings.setValue("FDmax", ui->FDmax->value());
}
