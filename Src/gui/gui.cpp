#include <QApplication>
#include <QMainWindow>

#include "mainwindow.h"

int main(int argc, char *argv[]) {
  QApplication app(argc, argv);
  /* To provide a uniform API, QSettings derives a fake domain name from the
   * organization name (unless the organization name already is a domain name,
   * e.g. OpenOffice.org). The algorithm appends ".com" to the company name and
   * replaces spaces and other illegal characters with hyphens. If you want to
   * specify a different domain name, call
   * QCoreApplication::setOrganizationDomain(),
   * QCoreApplication::setOrganizationName(), and
   * QCoreApplication::setApplicationName() in your main()function and then use
   * the default QSettings constructor*/
  QCoreApplication::setOrganizationName("tevian.ru");
  QApplication::setApplicationName("FaceCloud Client");

  MainWindow *mw = new MainWindow();

  mw->show();
  return app.exec();
}
