#ifndef SCREEN_H
#define SCREEN_H

#include "imagedata.h"
#include <QFrame>
#include <QImage>

#define MAX_ZOOM_FACTOR 4.5
class Screen : public QFrame {
  Q_OBJECT
private:
  int x0 = 0, x1 = 0, hMargin = 0;
  int y0 = 0, y1 = 0, vMargin = 0;

  double zoomFactor = 0.0, scaleFactor = 0.0;

  bool moving = false;
  QPoint lastPos;

  QImage image;
  QVector<FaceData> faces;

public:
  explicit Screen(QWidget *parent);

public slots:
  void setImage(ImageData imageData);

protected:
  void paintEvent(QPaintEvent *event) override;
  void resizeEvent(QResizeEvent *event) override;
  void mousePressEvent(QMouseEvent *event) override;
  void mouseReleaseEvent(QMouseEvent *event) override;
  void mouseMoveEvent(QMouseEvent *event) override;
#if QT_CONFIG(wheelevent)
  void wheelEvent(QWheelEvent *event) override;
#endif

private:
  void moveRefPoint(QPoint pos);
  void setMargins();
  void fixCoordinates(int &x, int &y);
};

#endif // SCREEN_H
