#include <QBuffer>
#include <QDir>
#include <QJsonDocument>
#include <QNetworkReply>
#include <QString>
#include <QUrlQuery>

#include "detector.h"

Detector::Detector(QString urlString, QString token)
    : urlString(urlString), token(token) {
  netManager = new QNetworkAccessManager(this);
}

void Detector::detect(QStringList files, int FDmin, int FDmax,
                      double threshold) {
  QFileInfoList list;

  for (QString file : files) {
    QFileInfo info(file);
    if (info.isFile() && info.isReadable())
      list.append(info);
  }
  perform(list, FDmin, FDmax, threshold);
}

void Detector::detect(QString folder, int FDmin, int FDmax, double threshold) {
  QDir dir(folder);
  QStringList filter;
  filter << "*.jpeg"
         << "*.jpg"
         << "*.png";
  QFileInfoList list = dir.entryInfoList(filter);
  perform(list, FDmin, FDmax, threshold);
}
#include <QThread>
void Detector::perform(QFileInfoList list, int FDmin, int FDmax,
                       double threshold) {
  emit counted(list.size());

  for (auto file : list) {
    QImage image;
    QString path = file.canonicalFilePath();
    image.load(path);
    request(path, image, FDmin, FDmax, threshold);
  }
}

void Detector::request(QString path, QImage image, int FDmin, int FDmax,
                       double threshold) {
  QNetworkRequest request;
  QNetworkReply *reply;
  QUrlQuery query;
  QUrl url(urlString);

  QByteArray imageBytes;
  QBuffer buffer(&imageBytes);

  query.addQueryItem("fd_min_size", QString::number(FDmin));
  query.addQueryItem("fd_max_size", QString::number(FDmax));
  query.addQueryItem("fd_threshold", QString::number(threshold));
  query.addQueryItem("demographics", "true");

  url.setQuery(query);
  request.setUrl(url);

  request.setRawHeader("Content-Type", "image/jpeg");
  request.setRawHeader("accept", "application/json");
  request.setRawHeader("Authorization",
                       QString("Bearer %1").arg(token).toUtf8());

  buffer.open(QIODevice::WriteOnly);
  image.save(&buffer, "JPEG");

  reply = netManager->post(request, imageBytes);

  connect(reply, &QNetworkReply::finished, this,
          [this, reply, path, image]() { handleReply(reply, path, image); });
}

void Detector::handleReply(QNetworkReply *reply, QString path, QImage image) {

  QByteArray body;
  QJsonDocument jsonDoc;
  QJsonObject faces;
  QJsonParseError parseError;

  auto error = reply->error();
  int code = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();

  if (code == 200) {
    body = reply->readAll();
    jsonDoc = QJsonDocument::fromJson(body, &parseError);
    if (jsonDoc.isNull() or jsonDoc.isArray()) {
      qWarning() << path << "\n"
                 << parseError.error << "\n"
                 << QString::fromUtf8(body);
      emit detectionFail();
    }

    else {
      faces = jsonDoc.object();
      emit obtained(path, faces, image);
    }

  } else {
    qWarning() << code << " : "
               << QString::fromUtf8(qvariant_cast<QByteArray>(reply->attribute(
                      QNetworkRequest::HttpReasonPhraseAttribute)));
    if (error != QNetworkReply::NoError)
      qWarning() << reply->errorString();
    if (code == 400 || code == 401)
      qWarning() << reply->readAll();
    emit detectionFail();
  }
  reply->close();
  reply->deleteLater();
}

void Detector::setToken(QString token) { this->token = token; };
