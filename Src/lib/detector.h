#ifndef DETECTOR_H
#define DETECTOR_H

#include <QFileInfo>
#include <QImage>
#include <QJsonObject>
#include <QNetworkAccessManager>
#include <QObject>

class Detector : public QObject {
  Q_OBJECT
public:
  explicit Detector(QString urlString = "", QString token = "");

signals:
  void counted(int num);
  void obtained(QString path, QJsonObject faces, QImage image);
  void detectionFail();

public slots:
  void detect(QStringList files, int FDmin, int FDmax, double threshold);
  void detect(QString folder, int FDmin, int FDmax, double threshold);
  void handleReply(QNetworkReply *reply, QString path, QImage image);
  void setToken(QString token);

private:
  void perform(QFileInfoList list, int FDmin, int FDmax, double threshold);
  void request(QString path, QImage image, int FDmin, int FDmax,
               double threshold);

  QNetworkAccessManager *netManager;
  QString urlString;
  QString token;
};

#endif // DETECTOR_H
