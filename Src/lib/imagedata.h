#ifndef IMAGEDATA_H
#define IMAGEDATA_H

#include <QImage>
#include <QtMath>
struct FaceData {
  double age = 0;
  QString gender;
  int x = 0, y = 0, height = 0, width = 0;

  void transform(FaceData &data, int dx, int dy, double scale,
                 double zoom) const {
    data.x = qFloor((x * scale + dx) * zoom);
    data.y = qFloor((y * scale + dy) * zoom);
    data.height = qFloor(height * scale * zoom);
    data.width = qFloor(width * scale * zoom);
    data.gender = gender;
    data.age = age;
  }
};

struct ImageData {
  QImage image;
  QString path;
  QVector<FaceData> faces;
};

Q_DECLARE_METATYPE(ImageData)

#endif // IMAGEDATA_H
