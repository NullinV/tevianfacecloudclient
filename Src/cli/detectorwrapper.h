#ifndef DETECTORWRAPPER_H
#define DETECTORWRAPPER_H

#include <QJsonObject>
#include <QObject>
#include <QTextStream>

class DetectorWrapper : public QObject {
  Q_OBJECT
public:
  explicit DetectorWrapper(QObject *parent = nullptr, QString url = "",
                           QString token = "");
  Q_INVOKABLE void detect(QStringList source, int FDmin, int FDmax,
                          double threshold);

signals:
  void detectFromFiles(QStringList files, int FDmin, int FDmax,
                       double threshold);
  void finished();
private slots:
  void printImageFaces(QString path, QJsonObject faces);

private:
  QString urlString;
  QString token;
  int leftFileNumber = 0;
  QTextStream cout;
};

#endif // DETECTORWRAPPER_H
