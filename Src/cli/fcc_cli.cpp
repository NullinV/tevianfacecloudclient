#include <QCommandLineParser>
#include <QCoreApplication>

#include "detectorwrapper.h"

int main(int argc, char *argv[]) {
  QCoreApplication a(argc, argv);
  QCoreApplication::setApplicationName("Tevian FaceCloud Client");

  QCommandLineParser parser;
  parser.setApplicationDescription("CLI detector");
  parser.addHelpOption();

  QCommandLineOption urlOption(QStringList() << "u"
                                             << "url",
                               "Face detector url", "string");

  parser.addOption(urlOption);

  QCommandLineOption sourceOption(QStringList() << "s"
                                                << "source",
                                  "Folder or file", "path");

  parser.addOption(sourceOption);

  QCommandLineOption tokenOption(QStringList() << "k"
                                               << "token",
                                 "Token", "string");
  parser.addOption(tokenOption);
  QCommandLineOption thresholdOption(QStringList() << "t"
                                                   << "threshold",
                                     "Threshold", "double");

  parser.addOption(thresholdOption);
  QCommandLineOption FDminOption(QStringList() << "i"
                                               << "FDmin",
                                 "FD Min", "int");
  parser.addOption(FDminOption);
  QCommandLineOption FDmaxOption(QStringList() << "a"
                                               << "FDmax",
                                 "FD max", "int");
  parser.addOption(FDmaxOption);

  parser.process(a);

  QString url = parser.value(urlOption);
  QString token = parser.value(tokenOption);
  double threshold = parser.value(thresholdOption).toDouble();
  int FDmin = parser.value(FDminOption).toInt();
  int FDmax = parser.value(FDmaxOption).toInt();
  QStringList source = parser.values(sourceOption);

  DetectorWrapper *detector = new DetectorWrapper(&a, url, token);
  QObject::connect(detector, &DetectorWrapper::finished, &a,
                   &QCoreApplication::quit);

  QMetaObject::invokeMethod(detector, "detect", Qt::QueuedConnection,
                            Q_ARG(QStringList, source), Q_ARG(int, FDmin),
                            Q_ARG(int, FDmax), Q_ARG(double, threshold));
  return a.exec();
}
