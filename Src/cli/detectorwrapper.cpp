#include <iostream>

#include <QDir>
#include <QJsonDocument>
#include <QThread>

#include "detector.h"
#include "detectorwrapper.h"

DetectorWrapper::DetectorWrapper(QObject *parent, QString url, QString token)
    : QObject(parent), urlString(url), token(token),
      cout(stdout, QIODevice::WriteOnly) {

  QThread *thread = new QThread(this);
  Detector *detector = new Detector(urlString, token);
  detector->moveToThread(thread);
  thread->start();

  connect(this, &DetectorWrapper::finished, thread, [thread]() {
    thread->quit();
    thread->wait();
  });
  connect(thread, &QThread::finished, detector, &Detector::deleteLater);

  connect(this, &DetectorWrapper::detectFromFiles, detector,
          qOverload<QStringList, int, int, double>(&Detector::detect));
  connect(detector, &Detector::counted, this, [this](int num) {
    if (!num) {
      cout << QString("There is nothing to perform\n");
      emit finished();
    }
    leftFileNumber = num;
  });
  connect(detector, &Detector::obtained, this,
          &DetectorWrapper::printImageFaces);
  connect(detector, &Detector::detectionFail, this,
          [this]() { leftFileNumber--; });
}

void DetectorWrapper::detect(QStringList source, int FDmin, int FDmax,
                             double threshold) {
  QStringList list;

  for (auto item : source) {
    QFileInfo info(item);
    if (info.isDir()) {
      QDir dir(item);
      QStringList filter;
      filter << "*.jpeg"
             << "*.jpg"
             << "*.png";
      QFileInfoList dList = dir.entryInfoList(filter);
      for (auto fInfo : dList)
        list.append(fInfo.absoluteFilePath());
    } else
      list.append(item);
  }
  emit detectFromFiles(list, FDmin, FDmax, threshold);
}

void DetectorWrapper::printImageFaces(QString path, QJsonObject faces) {
  cout << path << "\n";
  QJsonDocument doc(faces);
  QString strJson(doc.toJson(QJsonDocument::Indented));
  cout << strJson << "\n";
  if (!--leftFileNumber)
    emit finished();
}
